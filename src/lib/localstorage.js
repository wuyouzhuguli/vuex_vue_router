let local = {
  save (key, value) {
    localStorage.setItem(key, JSON.stringify(value))
  },
  get (key) {
    return JSON.parse(localStorage.getItem(key)) || {}
  },
  clear () {
    localStorage.clear()
  }
}

export default {
  install (vm) {
    vm.prototype.$db = local
  }
}
