import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// 定义一个容器
let store = new Vuex.Store({
  state: {
    count: 100
  },
  getters: {
    number (state) {
      return state.count > 110 ? 110 : state.count
    }
  },
  actions: {
    asyncMinus ({commit, dispatch}, n) {
      setTimeout(() => {
        commit('syncMinus', n)
        dispatch('hello')
      }, 2000)
    },
    asyncPlus ({commit, dispatch}, n) {
      setTimeout(() => {
        commit('syncPlus', n)
        dispatch('hello')
      }, 2000)
    },
    hello (state) {
      console.log('hello')
    }
  },
  mutations: {
    syncMinus (state, n) {
      state.count -= n
    },
    syncPlus (state, n) {
      state.count += n
    }
  }
})

export default store
