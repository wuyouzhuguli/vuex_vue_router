import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  linkActiveClass: 'is-active',
  // 点击浏览器前进后退，或者切换导航的时候触发
  scrollBehavior (to, from, savedPosition) {
    console.log(to)
    console.log(from)
    // 记录滚动条的坐标
    console.log(savedPosition)
    if (savedPosition) {
      return savedPosition
    } else if (to.hash) {
      return {
        selector: to.hash
      }
    } else {
      return {x: 0, y: 0}
    }
  },
  routes

})

router.beforeEach((to, from, next) => {
  console.log('导航进入前执行')
  if (to.matched.some((item) => item.meta.needLogin)) {
    // 判断是否已经登录
    let user = router.app.$db.get('user')
    if (user.isLogin) {
      next()
    } else {
      alert('这个页面需要登录才能访问哦😜')
      next('/login')
    }
  } else {
    next()
  }
})

router.afterEach((to, from) => {
  const title = to.meta.title
  if (title) {
    document.title = title
  } else {
    document.title = 'vue deep'
  }
})

export default router
