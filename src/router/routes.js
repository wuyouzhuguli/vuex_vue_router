import Home from '@/components/home'
import About from '@/components/about'
import Document from '@/components/document'
import PageNotFound from '@/components/PageNoFound'
import GitHub from '@/components/about/github'
import Blog from '@/components/about/blog'
import SideBar from '@/components/sidebar'
import TestHash from '@/components/testHash'
import Dynamic from '@/components/dynamic'
import Login from '@/components/login'
import Count from '@/components/count'
import Axios from '@/components/axios'

let routes = [
  {
    path: '/',
    component: Home,
    name: 'Home',
    meta: {
      needLogin: true,
      title: '首页'
    }
  }, {
    path: '/login',
    component: Login,
    meta: {
      title: '登录'
    }
  },
  // {
  //   path: '/index',
  //   name: 'Home',
  //   component: Home,
  //   alias: '/haha'
  // },
  {
    path: '/document',
    name: Document,
    components: {
      default: Document,
      sidebar: SideBar
    },
    meta: {
      title: '文档'
    }
  },
  {
    path: '/about',
    // name: 'About',
    component: About,
    meta: {
      title: '关于'
    },
    beforeEnter (to, from, next) {
      alert('查看关于MrBird的信息')
      next()
    },
    children: [
      // {
      //   path: '', // 默认子路由写法
      //   component: GitHub
      // },
      {
        path: 'github',
        component: GitHub,
        alias: '/github',
        meta: {
          title: 'GitHub'
        }
      },
      {
        path: 'blog',
        component: Blog,
        meta: {
          title: '博客'
        }
      }
    ]
  },
  {
    path: '/hash',
    component: TestHash,
    meta: {
      title: 'TestHash'
    }
  },
  {
    path: '/dynamic/:number?/:str?',
    component: Dynamic,
    meta: {
      title: 'Dynamic'
    }
  },
  {
    path: '/count',
    component: Count
  },
  {
    path: '/axios',
    component: Axios,
    meta: {
      title: 'Axios'
    }
  },
  {
    path: '*',
    component: PageNotFound,
    meta: {
      title: '404'
    }
    // redirect: '/index'
    // redirect: {
    //   path: '/index'
    // }
    // redirect: (to) => {
    //   console.log(to)
    //   return '/index'
    // }
  }
]

export default routes
