// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import db from './lib/localstorage'

Vue.config.productionTip = false
// Vue.prototype.$username = 'admin'
// Vue.prototype.$password = '123456'
// 或者这样自定义Vue属性

Vue.use({
  install (Vue, options) {
    Vue.prototype.$username = options.username
    Vue.prototype.$password = options.password
  }
},
{
  username: 'admin',
  password: '123456'
})

Vue.use(db)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template: '<App/>'
})
